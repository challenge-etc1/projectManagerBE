package org.mgiordano.security;

import io.quarkus.security.identity.SecurityIdentity;
import jakarta.annotation.Priority;
import jakarta.inject.Inject;
import jakarta.ws.rs.Priorities;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.Provider;

@Provider
@Priority(Priorities.AUTHENTICATION)
public class CSRFValidationFilter implements ContainerRequestFilter {

    @Inject
    SecurityIdentity securityIdentity;

    @Override
    public void filter(ContainerRequestContext requestContext) {
        if (!securityIdentity.isAnonymous()) {
            Object csrfTokenObj = securityIdentity.getAttribute("csrfToken");
            String csrfToken = csrfTokenObj != null ? csrfTokenObj.toString() : null;

            String csrfTokenHeader = requestContext.getHeaderString("X-CSRF-TOKEN");

            if (csrfToken == null || !csrfToken.equals(csrfTokenHeader)) {
                requestContext.abortWith(Response.status(Response.Status.FORBIDDEN)
                        .entity("Invalid CSRF token").build());
            }
        }
    }
}
