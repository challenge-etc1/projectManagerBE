package org.mgiordano.repositories;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;
import org.mgiordano.entities.Task;

@ApplicationScoped
public class TaskRepository implements PanacheRepository<Task> {
}
