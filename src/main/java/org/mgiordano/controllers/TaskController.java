package org.mgiordano.controllers;

import jakarta.annotation.security.RolesAllowed;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.core.Context;
import org.mgiordano.entities.Task;
import org.mgiordano.services.TaskService;

import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Path("/tasks")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@RolesAllowed({"user"})
public class TaskController {

    @Inject
    TaskService taskService;

    @POST
    public Response createTask(Task task) {
        taskService.createTask(task);
        return Response.status(Response.Status.CREATED).entity(task).build();
    }

    @GET
    @Path("/user/{userId}")
    public Response getTasksByUserId(@PathParam("userId") Long userId) {
        List<Task> tasks = taskService.getTasksByUserId(userId);
        return Response.ok(tasks).build();
    }

    @PUT
    @Path("/{id}")
    public Response updateTask(@PathParam("id") Long id, Task task) {
        return Response.ok(taskService.updateTask(id, task)).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteTask(@PathParam("id") Long id) {
        taskService.deleteTask(id);
        return Response.noContent().build();
    }

    @PUT
    @Path("/{id}/image")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response updateTaskImage(@PathParam("id") Long id, @Context HttpServletRequest request) throws IOException {
        InputStream inputStream = request.getInputStream();
        byte[] imageBytes = inputStream.readAllBytes();
        taskService.updateTaskImage(id, imageBytes);
        return Response.ok("Imagen actualizada correctamente").build();
    }

            @DELETE
            @Path("/{id}/image")
    public Response deleteTaskImage(@PathParam("id") Long id) {
        taskService.deleteTaskImage(id);
        return Response.noContent().build();
    }
}
