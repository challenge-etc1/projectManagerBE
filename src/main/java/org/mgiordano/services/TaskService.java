package org.mgiordano.services;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import org.mgiordano.entities.Task;
import org.mgiordano.repositories.TaskRepository;

import java.util.List;

@ApplicationScoped
public class TaskService {

    @Inject
    TaskRepository taskRepository;

    @Transactional
    public Task createTask(Task task) {
        taskRepository.persist(task);
        return task;
    }


    public List<Task> getTasksByUserId(Long userId) {
        return taskRepository.list("ownerId", userId);
    }

    @Transactional
    public Task updateTask(Long id, Task taskDetails) {
        Task task = taskRepository.findById(id);
        if (task != null) {
            task.title = taskDetails.title;
            task.description = taskDetails.description;
            task.status = taskDetails.status;
            task.ownerId = taskDetails.ownerId; // Ensure you handle user ownership properly
            task.image = taskDetails.image;
            taskRepository.persist(task);
            return task;
        } else {
            throw new IllegalStateException("Task not found");
        }
    }

    @Transactional
    public void deleteTask(Long id) {
        Task task = taskRepository.findById(id);
        if (task != null) {
            taskRepository.delete(task);
        } else {
            throw new IllegalStateException("Task not found");
        }
    }

    @Transactional
    public void updateTaskImage(Long id, byte[] imageBytes) {
        Task task = taskRepository.findById(id);
        if (task != null) {
            task.image = imageBytes;
            taskRepository.persist(task);
        } else {
            throw new IllegalStateException("Task not found");
        }
    }

    @Transactional
    public void deleteTaskImage(Long id) {
        Task task = taskRepository.findById(id);
        if (task != null && task.image != null) {
            task.image = null;
            taskRepository.persist(task);
        } else {
            throw new IllegalStateException("Task not found or no image to delete");
        }
    }
}
