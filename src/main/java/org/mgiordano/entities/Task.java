package org.mgiordano.entities;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.Column;
import jakarta.persistence.Lob;
import jakarta.persistence.ManyToOne;
import io.smallrye.common.constraint.NotNull;

@Entity
@Table(name = "tasks")
public class Task extends PanacheEntity {

    @NotNull
    public String title;

    public String description;

    @NotNull
    public String status;

    @Column(name = "owner_id")
    public Long ownerId;

    @Lob
    @Column(name = "image", columnDefinition="BLOB")
    public byte[] image;
}
